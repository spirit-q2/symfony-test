<?php

namespace AppBundle\Service;

use Symfony\Component\DomCrawler\Crawler;

class HttpRequest
{

    /**
     * @var resource
     */
    private $ch;

    public function __construct()
    {
        $this->ch = curl_init();
        curl_setopt_array($this->ch, [
            CURLOPT_HEADER => false,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_AUTOREFERER => true,
            CURLOPT_COOKIEFILE => 'cookie.txt',
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
        ]);
    }

    /**
     * @param $uri string
     *
     * @return bool|Crawler
     */
    public function get($uri)
    {
        curl_setopt($this->ch, CURLOPT_URL, $uri);

        $result = curl_exec($this->ch);
        if (false === $result) {
            return false;
        }

        return new Crawler($result);
    }
}