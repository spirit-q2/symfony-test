<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login_page")
     */
    public function loginAction(Request $request)
    {
        $helper = $this->get('security.authentication_utils');

        return $this->render('security/login.html.twig', [
            'email' => $request->request->get('email', ''),
            'error' => $helper->getLastAuthenticationError(),
        ]);
    }

    /**
     * @Route("/logout", name="logout_page")
     */
    public function logoutAction()
    {
        // Symfony never goes here, security system intercepts logout requests
    }

    /**
     * @Route("/register", name="register_page")
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        //$form = $this->createForm(UserType::class, $user); // php 5.5+
        $form = $this->createForm('AppBundle\Form\UserType', $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user
                ->setPassword($password)
                ->setConfirmKey(sha1(time()))
                ->setIsActive(0)
                ->setCurrentUri('');

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $messageBody = $this->renderView('emails/confirmation.html.twig', [
                'user' => $user
            ]);
            $message = \Swift_Message::newInstance()
                ->setSubject('Confirmation email')
                ->setFrom('admin@miracom.com.ua')
                ->setTo($user->getEmail())
                ->setBody($messageBody, 'text/html')
            ;
            $this->get('mailer')->send($message);

            $this->addFlash('success', 'Congratulations! A confirmation email was sent to your mailbox!');

            return $this->redirectToRoute('register_page');
        }

        return $this->render('security/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/confirm/{key}", name="confirm_page")
     */
    public function confirmAction($key = '')
    {
        $error = '';
        $success = false;

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:User');

        $user = $repository->findUserByKey($key);
        if (!$user) {
            $error = 'User is not found!';
        } else {
            $success = true;
            $user->setIsActive(1);
            $em->flush();
        }

        return $this->render('security/confirm.html.twig', [
            'error' => $error,
            'success' => $success,
        ]);
    }
}
