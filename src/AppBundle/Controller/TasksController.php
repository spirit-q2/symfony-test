<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\PostRepository;
use AppBundle\Service\HttpRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Client;

class TasksController extends Controller
{
    /**
     * @Route("/tasks", name="tasks")
     */
    public function tasksAction()
    {
        return $this->render('tasks/tasks.html.twig');
    }

    /**
     * @Route("/parse", name="parse")
     */
    public function parseAction()
    {
        /** @var HttpRequest $http */
        $http = $this->get('app.http_service');

        $em = $this->getDoctrine()->getManager();

        /** @var PostRepository $repository */
        $repository = $em->getRepository("AppBundle:Post");

        //получаем стартовую и еще две страницы
        $start = $http->get('http://wwwhatsnew.com');
        if (false === $start) {
            $this->addFlash('warning', 'Can\'t connect to wwwhatsnew.com!');
            return $this->render('tasks/tasks.html.twig');
        }

        $pages = [$start];
        $start->filter('.pagination a')->each(function(Crawler $a, $i) use (&$pages, $http) {
            if ($i >= 2) {
                return;
            }
            $pages[] = $http->get($a->attr('href'));
        });

        $counter = 0;

        for ($i = 0, $n = count($pages); $i < $n; $i++) {
            /** @var \DOMElement $article */
            foreach ($pages[$i]->filter('article[id]') as $article) {
                $c = new Crawler($article);

                $heading = $c->filter('h1.entry-title a');
                $url = $heading->attr('href');
                if ($repository->findPostByUrl($url)) {
                    continue;
                }

                $author = $c->filter('a.author-image')->attr('data-content');
                $title = $heading->html();
                $date = new \DateTime($c->filter('time.entry-date')->attr('datetime'));

                $body = $c->filter('.entry-content')->html();
                $image = '';
                if (preg_match('/<img.*?src=([\'"])([^\'"]+)\1[^\>]*>/', $body, $m)) {
                    $image = $m[2];
                    $body = str_replace($m[0], '', $body);
                }
                //убираем ссылки "подробнее"
                $body = preg_replace('/<a href="[^"]+" class="more-link">[^<]+<\/a>/', '', $body);
                //убираем пустые тэги
                $body = preg_replace('/<([^>]+\b)[^>]*>\s*<\/\1>/', '', $body);
                //убираем лишние стили
                $body = preg_replace('/style="[^"]+"/', '', $body);

                $body = trim($body);

                $post = new Post();
                $post->setUrl($url)
                    ->setTitle($title)
                    ->setAuthor($author)
                    ->setPostDate($date)
                    ->setImageUrl($image)
                    ->setBody($body);

                $em->persist($post);
                $em->flush();

                ++$counter;
                // var_dump([$url, $title, $author, $date->format('r'), $image, $body]);
            }
        }

        if ($counter > 0) {
            $this->addFlash('success', sprintf('Found %d new posts', $counter));
        } else {
            $this->addFlash('warning', 'No new posts so far');
        }

        return $this->redirectToRoute('tasks');
    }

    /**
     * @Route("/results", name="results")
     */
    public function resultsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository("AppBundle:Post");

        if ($request->isMethod('post')) {
            $range = $request->request->get('range');
            list($from, $to) = explode(' - ', $range);

            $from = new \DateTime($from.' 00:00:00');
            $to = new \DateTime($to.' 23:59:59');
            $posts = $repository->findByRange($from, $to);
        } else {
            $posts = $repository->findBy([], ['postDate' => 'DESC']);
            $last = current($posts);
            $first = end($posts);

            $range = sprintf(
                '%s - %s',
                $first->getPostDate()->format('Y-m-d'),
                $last->getPostDate()->format('Y-m-d')
            );
        }

        $chart = [];
        $chartArray = $repository->getChart();
        for ($i = 0, $n = count($chartArray); $i < $n; $i++) {
            $chart[$chartArray[$i]['group_post_date']] = $chartArray[$i]['total'];
        }

        return $this->render('tasks/results.html.twig', [
            'posts' => $posts,
            'range' => $range,
            'chart' => $chart,
        ]);
    }
}
