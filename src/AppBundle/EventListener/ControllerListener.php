<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class ControllerListener
{
    public function onKernelController(FilterControllerEvent $event)
    {
        // app.request.attributes.get('_route')
        $request = $event->getRequest();
        if ($request->isXmlHttpRequest() || !$event->isMasterRequest()) {
            return;
        }

        /** @var Controller $controller */
        $controller = $event->getController()[0];
        if ($controller instanceof Controller) {
            /** @var User $user */
            $user = $controller->get('security.token_storage')->getToken()->getUser();
            if (is_object($user) && in_array('ROLE_USER', $user->getRoles())) {
                $user->setCurrentUri($request->getRequestUri());
                $controller->getDoctrine()->getManager()->flush();
            }
        }
    }
}